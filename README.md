# gturnquist-quoters

This is a clone of https://github.com/spring-guides/quoters with a `Dockerfile` added. It is used by funtional tests in https://bitbucket.org/c967784/spring-boot-coding-exercise 

## Reference

Spring Blog: https://spring.io/blog/2014/08/21/building-a-restful-quotation-service-with-spring
Source: https://github.com/spring-guides/quoters

## Build docker container

```
docker build -t gturnquist-quoters .
```

## Run docker container

```
docker run -p 8080:8080 --rm --name quoters gturnquist-quoters
```
